import cv2
import numpy as np
from detectorFun import Detector

cap=cv2.VideoCapture(0)
while True:
    _, img = cap.read()
    #metodo para hacer el procesamiento datos como array 
    datos,img,mask = Detector(blue=1,img=img).get_frame()
    
    print (datos)
    cv2.imshow("mask", mask)
    cv2.imshow("Imgg", img)
    key = cv2.waitKey(1)
    if key == 27:
        break
cap.release()
cv2.destroyAllWindows()