import cv2
import numpy as np

class Detector:
    def lados(self,c):
        nc=cv2.convexHull(c)
        approx = cv2.approxPolyDP(c,0.02*cv2.arcLength(nc,True),True)
        return len(approx)

    def datos(self,c,img):
        self.datosDir = []
        nc=cv2.convexHull(c)
        moments = cv2.moments(nc)
        if moments['m00']==0:moments['m00']=1
        x = int(moments['m10']/moments['m00'])
        y = int(moments['m01']/moments['m00'])
        font= cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img,'{},{}'.format(x,y),(x+10,y),font,0.75,(255,0,0),1,cv2.LINE_AA)
        cv2.circle(img, (x, y), 4,(255,255,255), 2)
        cv2.drawContours(img,[nc],0,(255,255,255),2)
                    
        if (int(img.shape[0]/2) < y):
            cv2.putText(img,'Abajo',(10,10),font,0.75,(255,0,0),1,cv2.LINE_AA)
            self.datosDir.append("Abajo")
        elif(int(img.shape[0]/2) > y):
            cv2.putText(img,'Arriba',(10,10),font,0.75,(255,0,0),1,cv2.LINE_AA)
            self.datosDir.append("Arriba")
        else:
            self.datosDir.append("0")

        if (int(img.shape[1]/2) < x):
            cv2.putText(img,'Derecha',(10,40),font,0.75,(0,255,0),1,cv2.LINE_AA)
            self.datosDir.append("Derecha")
        elif(int(img.shape[1]/2) > x):
            cv2.putText(img,'Izquierda',(10,40),font,0.75,(0,255,0),1,cv2.LINE_AA)
            self.datosDir.append("Izquierda")
        else:
            self.datosDir.append("0")

    def __init__ (self,blue):
        self.cap = cv2.VideoCapture(0)
        self.blue = blue
        self.datosDir = []
        
    def __del__(self):
        #releasing camera
        self.cap.release()
    
    def get_frame(self):
        ##Rangos de color que selecciona
        #Azules
        
        sensitivity = 30;

        lower_blue = np.array([114 - sensitivity,65,75])
        upper_blue = np.array([114 + sensitivity, 255, 255])

        #Verde (solo descomenta para detectar verde en vez de azul)
        #lower_blue = np.array([60 - sensitivity, 100, 50] )
        #upper_blue = np.array([60 + sensitivity, 255, 255] )

        # define range of red color in HSV
        lower_red1 = np.array([0,65,75])
        upper_red1 = np.array([7, 255, 255])
        lower_red2 = np.array([172,65,75])
        upper_red2 = np.array([179, 255, 255])
        blue = self.blue
        #blue=0
            
        

        while True:    
            timer = cv2.getTickCount()
            _, img = self.cap.read()
            imgg=cv2.cvtColor(img,cv2.COLOR_BGR2HSV) #convertir imagen a otro color
            if blue:
                mask = cv2.inRange(imgg, lower_blue, upper_blue)
            else:
                # Threshold the HSV image to get only blue colors
                mask1 = cv2.inRange(imgg, lower_red1, upper_red1)
                mask2 = cv2.inRange(imgg, lower_red2, upper_red2)
                mask = cv2.add(mask1, mask2)
            kernel = np.ones((5,5),np.float32)/25
            mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
            mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
            #mask = cv2.filter2D(mask,-1,kernel)
            #blur = cv2.GaussianBlur(mask, (5, 5),0)
            edges = cv2.Canny(mask,100,200)
            #laplacian = cv2.Laplacian(edges,cv2.CV_8UC1,1)
            
            contours,hierarchy = cv2.findContours(edges,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
            
            areas = [[c,cv2.contourArea(c)] for c in contours if self.lados(c)==4 if cv2.contourArea(c) > 200]
            if areas:
                areas = sorted(areas,key=lambda x: x[1])
                self.datos(areas[-1][0],img) 
                    
            res = cv2.bitwise_and(img,img, mask= mask)

            #Centro de la imagen
            cv2.circle(img, (int(img.shape[1]/2),int(img.shape[0]/2)), 4,(255,255,255), 2)

            #imprimir FPS
            fps = cv2.getTickFrequency()/(cv2.getTickCount()-timer)
            cv2.putText(img,str(int(fps)),((int(img.shape[1]-80)),int(img.shape[0]-10)),cv2.FONT_HERSHEY_SIMPLEX,0.75,(255,255,0),2)
            #cv2.imshow("Frame", res)
            

            cv2.imshow("Img", img)
            cv2.imshow("mask", mask)
            #cv2.imshow("Imgg", imgg)
            # encode OpenCV raw frame to jpg and displaying it
            #return(self.datosDir)
            key = cv2.waitKey(1)
            if key == 27:
                break
        #self.cap.release()
        #cv2.destroyAllWindows()


Detector(blue=1).get_frame()
